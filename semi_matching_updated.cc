#include <list>
#include <set>
#include <string>
#include <limits>
#include <map>
#include <deque>
#include <iostream>
#include <fstream>
#include <sstream>
#include "bgp.hpp"

typedef std::map<int, int> semi_map;
typedef std::map<int, Node> node_map;

struct Edge {	
	
	int edge_AS; // next AS
	unsigned int cost; // positive edge cost
	
	Edge (int e, unsigned int c) {
		edge_AS = e;
		cost = c;
	}
};

class Node {
	
	public:
		int ASN;  // AS number
		int type; // destination (1) or neighbor (2)
        unsigned int weight;  // positive weight
        bool matched; // included in semi-matching or not
        unsigned int degree_matching; // semi-matching degree
        std::list<Edge> reachable; // neighbors in bipartite graph
        int parent; //parent ASN in BFS
        //Node *parent; // parent in BFS
        unsigned int cost_increment; // w_P for the min weight load-balanced
                                     // semi-matching
		
		Node () {
			ASN = -1;
		};
		
		Node (int num, int cat, int wt) {
			ASN = num;
			type = cat;
			matched = false;
			cost_increment = std::numeric_limits<unsigned int>::max();
			degree_matching = 0;
			weight = wt;
			parent = -1;
			//parent = NULL;
		}
};

class NodeCompare {

  public:	
	bool operator () (Node n1, Node n2) const {
		bool val = (n1.ASN < n2.ASN);
		return val;
	}
};

semi_map load_balanced (node_map Destinations, node_map Neighbors) {
	
	std::list<Node> BFS_queue;
	std::map<int, Node>::iterator dit;
	std::list<Edge>::iterator eiter;
	std::map<int, int> matching; // final semi-matching
	//std::set<Node, NodeCompare> visited; // nodes visited during a BFS
	std::set<int> visited; //TODO: make visited just a set of ints -- don't play with references
	
	for (dit = Destinations.begin(); dit != Destinations.end(); dit++) {
	  Node dest = (*dit).second;
	  std::cout<<"Node choosed: " << dest.ASN << std::endl;
	  for (std::set<int>::iterator iter = visited.begin(); iter != visited.end(); iter++) {
		  if (Destinations.count (*iter) > 0)
				Destinations[*iter].parent = -1;
		  else
				Neighbors[*iter].parent = -1;
	}
	  visited.clear ();
	  Node best_router;
	  visited.insert (dest.ASN);
	  BFS_queue.push_back (dest);
	  
	  while (!BFS_queue.empty ()) {
		
		Node next = BFS_queue.front ();
		//printf ("who is popped? %d \n", next.ASN);
		BFS_queue.pop_front ();
		std::list<Edge> BFS_neighbors;
		for (eiter = next.reachable.begin(); eiter != next.reachable.end(); eiter++) {
			struct Edge e = *eiter;
			if (next.type == 1) {
				if (!next.matched || !Neighbors[e.edge_AS].matched)
					BFS_neighbors.push_back (e);
				else if (matching[next.ASN] != e.edge_AS)
					BFS_neighbors.push_back (e);
			}
			else if (matching.count (e.edge_AS) > 0 && matching[e.edge_AS] == next.ASN)
					BFS_neighbors.push_back (e);
			}
			
		if (next.type == 2 && 
			(best_router.ASN < 0 || (next.degree_matching < best_router.degree_matching)))	
				best_router = next;
		
		for (eiter = BFS_neighbors.begin(); eiter != BFS_neighbors.end(); eiter++) {
			Node adj;
			if (next.type == 1)
				adj = Neighbors[(*eiter).edge_AS];
			else
				adj = Destinations[(*eiter).edge_AS];
			if (visited.count (adj.ASN) == 0) {
				adj.parent = next.ASN;
				if (next.type == 1)
					Neighbors[(*eiter).edge_AS].parent = next.ASN;
				else
					Destinations[(*eiter).edge_AS].parent = next.ASN;
				visited.insert (adj.ASN);
				BFS_queue.push_back (adj);	
			}
		}
		
	} 
	    //printf ("come put\n");
        Node end = best_router;
        //printf ("best router is %d with parent %d\n", end.ASN, end.parent);
        Node start = Destinations[end.parent];
        Destinations[end.parent].matched = true;
		matching[start.ASN] = end.ASN;
		Neighbors[best_router.ASN].degree_matching++;
		Neighbors[best_router.ASN].matched = true;
		Destinations[end.parent].degree_matching++;
		
		while (start.ASN != dest.ASN) {
			//printf ("iterating with %d \n", start.ASN);
			Destinations[end.parent].degree_matching--;
			end = Neighbors[start.parent];
			Neighbors[start.parent].matched = true;
			start = Destinations[end.parent];
			matching[start.ASN] = end.ASN;
			Destinations[end.parent].degree_matching++;
			Destinations[end.parent].matched = true;
		}
		
 }
    /*
	std::map<int, int>::iterator match_iter; // iterate through destinations
	for (match_iter = matching.begin();match_iter != matching.end(); match_iter++)
		printf ("Destination %d is routed through neighbor %d \n", match_iter->first, match_iter->second); 
    */
	return matching;
}

semi_map min_weight_load_balanced (node_map Destinations, node_map Neighbors) {
	
	std::list<Node> BFS_queue;
	std::map<int, Node>::iterator dit; // iterate through nodes
	std::list<Edge>::iterator eit; // iterate through edges
	std::set<int>::iterator iter; // clear state from previous BFS
	std::map<int, int> matching; // final semi-matching
	//std::set<Node, NodeCompare> visited; // nodes visited during a BFS
	std::set<int> visited;
	std::set<int> pre_visited;
	
	for (dit = Destinations.begin(); dit != Destinations.end(); dit++) {
	  Node dest = dit->second;
	  for (iter = visited.begin(); iter != visited.end(); iter++) {
		  if (Destinations.count (*iter) > 0) {
				Destinations[*iter].parent = -1;
				Destinations[*iter].cost_increment = std::numeric_limits<unsigned int>::max();
			}
		  else {
				
				Neighbors[*iter].parent = -1;
				Neighbors[*iter].cost_increment = std::numeric_limits<unsigned int>::max();
			}	
	}
	  visited.clear ();
	  pre_visited.clear ();
	  dest.cost_increment = 0;
	  Node best_router;
	  std::cout<<"Node choosed: " << dest.ASN << std::endl;
	  BFS_queue.push_back (dest);
	  	  
	  while (!BFS_queue.empty ()) {
		
		Node next = BFS_queue.front ();
		BFS_queue.pop_front ();
		//if (next.type == 1 && (next.cost_increment > Destinations[next.ASN].cost_increment))
			//continue;
		//if (next.type == 2 && (next.cost_increment > Neighbors[next.ASN].cost_increment))
			//continue;
		if (next.type == 1) {
			next.cost_increment = Destinations[next.ASN].cost_increment;
			next.parent = Destinations[next.ASN].parent;
		}
		else {
			next.cost_increment = Neighbors[next.ASN].cost_increment;
			next.parent = Neighbors[next.ASN].parent;
		}
		
		visited.insert (next.ASN);
		pre_visited.erase (next.ASN);
		std::list<Edge> BFS_neighbors;
		for (eit = next.reachable.begin(); eit != next.reachable.end(); eit++) {
			struct Edge e = *eit;
			if (next.type == 1) {
				if (!next.matched || !Neighbors[e.edge_AS].matched)
					BFS_neighbors.push_back (e);
				else if (matching[next.ASN] != e.edge_AS)
					BFS_neighbors.push_back (e);
			}
			else if (matching.count (e.edge_AS) > 0 && matching[e.edge_AS] == next.ASN)
					BFS_neighbors.push_back (e);
		}
		
		if (next.type == 2) {
			if (best_router.ASN < 0)
				best_router = next;
		    else if (next.degree_matching < best_router.degree_matching)
				best_router = next;
			else if ((next.degree_matching == best_router.degree_matching)
					&& (next.cost_increment < best_router.cost_increment))
				best_router = next;
		}
		    
		for (eit = BFS_neighbors.begin(); eit != BFS_neighbors.end(); eit++) {
			unsigned int cost = (*eit).cost;
			unsigned int temp_cost;
			if (next.type == 1)
			      temp_cost = next.cost_increment + cost;
			else
			      temp_cost = next.cost_increment - cost;
	
			Node adj;
			if (next.type == 1)
				adj = Neighbors[(*eit).edge_AS];
			else
				adj = Destinations[(*eit).edge_AS];
			
			if (temp_cost < adj.cost_increment) {
				if (next.type == 1) {
					Neighbors[(*eit).edge_AS].cost_increment = temp_cost;
					Neighbors[(*eit).edge_AS].parent = next.ASN;
				}
		        else {
					Destinations[(*eit).edge_AS].parent = next.ASN;
					Destinations[(*eit).edge_AS].cost_increment = temp_cost;
				}
				adj.cost_increment = temp_cost;
				adj.parent = next.ASN;
				if (visited.count (adj.ASN) > 0) {
					visited.erase (adj.ASN);
					BFS_queue.push_back (adj);
				}
				else if (pre_visited.count (adj.ASN) == 0) {
					pre_visited.insert (adj.ASN);
					BFS_queue.push_back (adj);
				}
		}
									
			}
		} 

		Node end = best_router;
        printf ("best router is %d with parent %d\n", end.ASN, end.parent);
        Node start = Destinations[end.parent];
        Destinations[end.parent].matched = true;
		matching[start.ASN] = end.ASN;
		Neighbors[best_router.ASN].degree_matching++;
		Neighbors[best_router.ASN].matched = true;
		Destinations[end.parent].degree_matching++;
		while (start.ASN != dest.ASN) {
			printf ("iterating with %d \n", start.ASN);
			Destinations[end.parent].degree_matching--;
			end = Neighbors[start.parent];
			Neighbors[start.parent].matched = true;
			start = Destinations[end.parent];
			matching[start.ASN] = end.ASN;
			Destinations[end.parent].matched = true;
			Destinations[end.parent].degree_matching++;
		}
		
 }
 
	/*
	std::map<int, int>::iterator match_iter; // iterate through destinations
	for (match_iter = matching.begin();match_iter != matching.end(); match_iter++)
		printf ("Destination %d is routed through neighbor %d \n", match_iter->first, match_iter->second); 
   **/
	return matching;
}

void write_to_file (int start_dest, int end_dest, route_t rtable, int src_AS, int sm_case) {
	
	std::map<int,std::deque<int> > diverse_routes = rtable[src_AS];
	std::map<int, std::deque<int> > ::iterator mit;
	std::ofstream outfile;
	std::stringstream combine;
	outfile.open ("routes2.dat");
	for (mit = diverse_routes.begin(); mit != diverse_routes.end(); mit++) {
		int dest_AS = mit->first;
		std::deque<int> path = mit->second; // chosen path to destination
		outfile<<"Destination: "<<dest_AS<<"  Path: ";
		for (std::deque<int>::iterator n_mit = path.begin(); n_mit != path.end (); ++n_mit)
			outfile<<(*n_mit)<<" ";
		outfile<<std::endl;
		
	}
	
	outfile.close ();	
}

/*
route_t
read_from_file (int sm_case, int start_dest, int end_dest, int src_AS) {
	
   std::ifstream infile;
   std::stringstream combine;
   combine<< "case:" << sm_case << "_" << start_dest << "_" << end_dest;
   infile.open (combine.str (), std::ifstream::in);
   std::string line;
   route_t route_table;
   std::map<int, std::deque<int> > routes;
   while (getline (infile, line)) {
	std::istringstream iss (line);
	std::string AS;
	int i = 0;
	int dest_AS;
	std::deque<int> droute; 
	while (std::getline (iss, AS, " ")) {
	   if (AS.compare ("") == 0)
			break;
	   if (i == 0 || i == 2) {
		   i++;
		   continue;
	   }
	   if (i == 1) {
		dest_AS = atoi(AS.c_str());
		i++;
		continue;
	}
	   	droute.push_back (atoi(AS.c_str()));
   }
   routes[dest_AS] = droute;
}
   infile.close();
   route_table[src_AS] = routes;
   return route_table;	
}
*/

route_t generate_routes (std::multimap<int, std::deque<int> > rib_entry,  int scenario, std::set<int> restrict, int src_AS) {
	
	//create bipartite graph
	std::multimap<int,std::deque<int> >::iterator mit; // multimap iterator
    std::pair<std::multimap<int,std::deque<int> >::iterator,std::multimap<int,std::deque<int> >::iterator> ret;
    
    std::map<int, Node> Destinations;
    std::map<int, Node> Neighbors;
    // all the destinations reachable for this router
    for (mit = rib_entry.begin(); mit != rib_entry.end(); mit=rib_entry.upper_bound (mit->first)) {
      ret = rib_entry.equal_range(mit->first); // all the paths to a single destination
      int dest_AS = mit->first;
      
      if (restrict.count (dest_AS) == 0)
	  	continue;
      Destinations[dest_AS] = Node (dest_AS, 1, 0);
      //std::cout<<"destination: "<< dest_AS << std::endl;
      for(std::multimap<int,std::deque<int> >::iterator n_mit = ret.first; n_mit != ret.second; ++n_mit) {
		std::deque<int> routers = n_mit->second;
		int neigh_AS = routers.front ();
		if (Neighbors.count (neigh_AS) == 0)
			Neighbors[neigh_AS] = Node (neigh_AS, 2, 0);
		bool exists = false;
		for (std::list<Edge>::iterator eit = Destinations[dest_AS].reachable.begin ();
			eit != Destinations[dest_AS].reachable.end (); eit++) {
				if (eit->edge_AS == neigh_AS) {
					exists = true;
					break;
				}
		}
		
		if (exists)
			continue;
		// add edge
		// cost for now is just AS PATH length
		struct Edge e1 (dest_AS, routers.size ());
		Neighbors[neigh_AS].reachable.push_back (e1);
		struct Edge e2 (neigh_AS, routers.size ());
		Destinations[dest_AS].reachable.push_back (e2);
	}
 }
	/*
	std::map<int, Node>::iterator check;
	std::list<Edge>::iterator ec;
	for (check = Destinations.begin (); check != Destinations.end (); check++) {
		printf ("Destination is %d\n", check->first);
		Node dest = check->second;
		for (ec = dest.reachable.begin(); ec != dest.reachable.end (); ec++) 
			printf ("neighbor is %d and cost is %d \n", ec->edge_AS, ec->cost);
		printf ("\n");
	}
	for (check = Neighbors.begin (); check != Neighbors.end (); check++) {
		printf ("Neighbor is is %d\n", check->first);
		Node dest = check->second;
		for (ec = dest.reachable.begin(); ec != dest.reachable.end (); ec++) 
			printf ("destination is %d and cost is %d \n", ec->edge_AS, ec->cost);
		printf ("\n");
	}
	*/
	std::map<int, int> routes;
	
	//call appropriate semi-matching case
	switch (scenario) {
	
	case 1  :   // min. weight load balanced
				routes =  min_weight_load_balanced (Destinations, Neighbors);
				break;
				
	case 2  :   // unweighted load balanced
				routes = load_balanced (Destinations, Neighbors);
	}
	
	route_t route_table;
	std::map<int, std::deque<int> > diverse_routes;
	for (mit = rib_entry.begin(); mit != rib_entry.end(); mit=rib_entry.upper_bound (mit->first)) {
		int dest_AS = mit->first;
		if (restrict.count (dest_AS) == 0)
			continue;
		ret = rib_entry.equal_range(mit->first); // all the paths to a single destination
		for (std::multimap<int,std::deque<int> >::iterator n_mit = ret.first; n_mit != ret.second; ++n_mit) {
			std::deque<int> routers = n_mit->second;
			int neigh_AS = routers.front ();
			if (diverse_routes.count (dest_AS) == 0 && routes[dest_AS] == neigh_AS) {
				diverse_routes[dest_AS] = routers;
				break;
			}
		}
	}
			
	//return chosen routes
	route_table[src_AS] = diverse_routes;
	return route_table;
}

semi_map double_sorted(rib_t rib_table){
			
  // Read INRIA, semi-matching algo for scheduling || tasks under resrc constraints
  // Code to create a degree map for neighbors
  // Should be moved out of this procedure
  std::map<int, int>  asn_deg_hash;

  rib_t::iterator rib_it;
  mpath_t::iterator mpath_it;
  mpath_t::iterator hash_it;
  std::pair<mpath_t::iterator, mpath_t::iterator> ret;

  for (rib_it = rib_table.begin(); rib_it != rib_table.end(); ++rib_it){
    for (mpath_it = rib_it->second.begin(); mpath_it != rib_it->second.end(); ++mpath_it){
      ret = mpath_it->second.equal_range(mpath_it->first);
      for(hash_it = ret.first; hash_it != ret.second ; ++hash_it){
	if (asn_deg_hash.find(hash_it->second[0]) != asn_deg_hash.end()){
	++asn_deg_hash[hash_it->second[0]];
      }
      else{
	asn_deg_hash[hash_it->second[0]] = 1;
      } 
      }
    }
  }

  std::map<int,int>::iterator it;
  std::vector<int> sorted_dest;
  for(it = asn_deg_hash.begin(); it != asn_deg_hash.end(); ++it){
    

  // n in the paper refers to the total no of nodes
  // we are a bit generous, the value is decreasing whatever initial value is chosen
  int minl = asn_deg_hash.size(); // load
  int mind = asn_deg_hash.size(); // degree

  for (rib_it = rib_table.begin(); rib_it != rib_table.end(); ++rib_it){
    for(mpath_it = rib_it->second.begin(); 
	mpath_it = rib_it->second.end(); ++rib_it){

      ret = mpath_it->second.equal_range(mpath_it->first);
      for(hash_it = ret.first; hash_it != ret.second; ++hash_it){
	if ((mpath_it->second.count(mpath_it->first) < minl) || 
	    ((mpath_it->second.count(mpath_it->first) ==  minl) &&
	     (asn_deg_hash[hash_it->
      }

    }
 
  }

}
int
main (int argc, char *argv[]) {
  
  rib_t rib;
  rib = make_rib_table(rib);
  //print_stats (rib);
  int src_AS = 12654;
  int start_dest = 200;
  int end_dest = 300;
  std::set<int> interest;
  for (int i = start_dest; i<= end_dest; i++)
	interest.insert (i);
  /*	
  interest.insert (64800);
  interest.insert (65007);
  interest.insert (64601);
  interest.insert (63354);
  interest.insert (64549);
  */
  route_t rtable = generate_routes (rib[src_AS], 1, interest, src_AS);
  write_to_file (start_dest, end_dest, rtable, src_AS, 1);
  
  std::map<int, std::deque<int> > diverse_routes = rtable[src_AS];
  std::map<int, std::deque<int> >::iterator mit;
  
  for (mit = diverse_routes.begin(); mit != diverse_routes.end(); mit++) {
		int dest_AS = mit->first;
		std::deque<int> path = mit->second; // chosen path to destination
		std::cout<<"Destination: "<<dest_AS<<" Path: ";
		for (std::deque<int>::iterator n_mit = path.begin(); n_mit != path.end (); ++n_mit)
			std::cout<<(*n_mit)<<" ";
		std::cout<<std::endl;
		
	}
  	
  return 0;
}
