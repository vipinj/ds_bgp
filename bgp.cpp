#include <iostream>
#include "bgp.hpp"
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <set>


int main(){
  rib_t rib;
  route_t route;
  rib = make_rib_table(rib);
  std::cout<<"Rib table complete"<<std::endl;
  // print_rib_table(rib);

  route = upd_route_table_norm(rib,route);
  print_stats(rib);
  route_t::iterator it;
  for(it = route.begin();it!= route.end(); ++it){
    std::cout<<"Route table size "<<it->second.size()<<std::endl;
  }
  std::cout<<"Route table complete"<<std::endl;
  reachability_metric(route);
  reachability_metric_failure(route,1836);  
  avg_path_length(route);
  return 0;

}
