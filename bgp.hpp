
#include <iostream>
#include <map>
#include <vector>
#include <algorithm>
#include <deque>
#include <fstream>
#include <string>
#include <sstream>
#include <set>

// struct node {
//   int _node;
//   int _cost;
// };

// class as_graph {
//   struct node;
// };

// class node{
//   int _id;
//   int _cost;
// };

// class edge{
//   int _cost;
//   edge *next;
// };

//Node and dest,path(multiple paths to a dest)
typedef std::map<int,std::deque<int> > path_t;
typedef std::multimap<int,std::deque<int> > mpath_t;
typedef std::map<int, mpath_t > rib_t;
// Node and chosen path to the destination
typedef std::map<int, path_t > route_t;

// Contains the pair <nodeA, nodeB, cost> and <nodeB, nodeA, cost> for peers-relation, otherwise only 
// a single directional tuple
std::map<int,std::map<int, int> > cost_table; 



route_t upd_route_table_norm(rib_t rib_table, route_t route_table){
  rib_t::iterator it;
  mpath_t::iterator mit; // multimap iterator
  mpath_t::iterator n_mit;  // route update iterator
  std::pair<mpath_t::iterator,mpath_t::iterator> ret;
  path_t temp_map;  
  path_t  temp_map2;  

  // all the sources in rib table
  for (it = rib_table.begin();it!= rib_table.end();++it){ 

    // all the destination in the secondary rib table
    for(mit = it->second.begin(); mit != it->second.end(); ++mit){
      // all the paths to a single destination
      ret = it->second.equal_range(mit->first); 
      int min_len(100); // 100 is a random choice for path length
      // worst case for this algo is O(n) where n is the number of multiple paths
      for(n_mit = ret.first; n_mit != ret.second;++n_mit){
	int len = n_mit->second.size();
	if(len < min_len){
	  min_len = len;
	  int destination = mit->first;
	  std::deque<int> path = n_mit->second;
	  temp_map[destination] = path;
	}
      }
    }
  }

  std::cout<<"Number of destinations are"<<temp_map.size()<<std::endl;
  for(int j = 1; j < 1001; ++j){
    temp_map2[j] = temp_map[j];

    if(temp_map[j].size()){
      std::cout<<"Destination: "<<j<<" Path: ";
      for(int k = 0; k < temp_map[j].size() ; ++k){
    	std::cout<<temp_map[j][k]<<" ";
      }
      std::cout<<std::endl;
    }
  }
  route_table[it->first] = temp_map2;  // temporarily changing it to cater to 200 to 500 nodes
  return route_table ;
}

rib_t make_rib_table(rib_t rib_table){
  std::ifstream data;
  mpath_t temp_mmap;
  std::pair<mpath_t::iterator,mpath_t::iterator> ret;
  mpath_t::iterator it;
  std::string str2;
  data.open("./bview_table3",std::ifstream::in);
  std::deque<int> deq;
  int temp(0),count(0);
  int i;
  int source_asn(12654); // Source ASN, not 3333
  while(getline(data,str2)){
    std::stringstream str1(str2);
    int j(0);
    // Entries like A A B B B C C C C
    while(str1 >> i){ // is this a good practice?, verify
      if(j == 0){
	deq.push_back(i);
	++j;
	continue;
      }
      if(deq[j-1] != i){
      deq.push_back(i);
      // std::cout<<i<<" ";
      ++j;
      }

    }
    // For cases like route A-B and A-B-B(with B removed), 
    // multimap makes duplicate entries unlike map
    //Naive way which first checks the size of 
    // deque(similar keys) and then compare with each

    if(j > 0){  // If there is a blank line in the data file
      ret = temp_mmap.equal_range(deq[j-1]);
      for(it = ret.first;it!= ret.second;++it){ 
	if(deq.size() == it->second.size()){  // check if deq sizes are equal
	  if(deq == it->second){
	    deq.clear();
	    continue;
	  }
	}
      }
      if(deq.size() > 0){
	temp_mmap.insert(std::make_pair(deq[j-1],deq));
	count+= 1;
	// if(deq[j-1] == 291){
	//   std::cout<<deq[0]<<" "<<deq[1]<<std::endl;
	// }
      }
    }
    deq.clear();
  }
  rib_table[source_asn] = temp_mmap;
  //  std::cout<<source_asn<<std::endl;
  std::cout<<"Unique Routes: "<<count<<std::endl;
  return rib_table;
}

void print_rib_table(rib_t rib_table){
  rib_t::iterator it;
  mpath_t::iterator mit;
  std::deque<int>::iterator dit;
  std::set<int> uniq_dest;

  for(it = rib_table.begin();it != rib_table.end();++it){
    std::cout<<it->first<<" "<<it->second.size() <<" ";
    for(mit = it->second.begin();mit!= it->second.end();++mit){
      std::cout<<mit->first<<" "<< mit->second.size()<<std::endl;
      uniq_dest.insert(mit->first);
      for(dit = mit->second.begin(); dit != mit->second.end(); ++dit){
	std::cout<<*dit<<" ";
    }
      std::cout<<std::endl;
    }
  }
  std::cout<<"Total unique destinations: "<<uniq_dest.size()<<std::endl;
  return ;
}

void print_stats(rib_t rib_table){
  std::set<int> uniq;
  std::set<int> uniq_dest;
  int max_count(0),max_asn(0);
  rib_t::iterator it;
  mpath_t::iterator mit;
  std::deque<int>::iterator dit;

  for(it = rib_table.begin();it != rib_table.end();++it){
    std::cout<<it->first<<" "<<it->second.size() <<" ";
    for(mit = it->second.begin();mit!= it->second.end();++mit){
      uniq_dest.insert(mit->first);
      if(uniq.find(mit->first) == uniq.end()){
	uniq.insert(mit->first);
	// std::cout<<mit->first<<" Count: "<<it->second.count(mit->first)<<std::endl;
	if(max_count < it->second.count(mit->first)){
	  max_count = it->second.count(mit->first);
	  max_asn = mit->first;
	}
      }
    }
  }
  std::cout<<"Count: "<<max_asn<<" "<<max_count<<std::endl;
  std::cout<<"Total unique destinations: "<<uniq_dest.size()<<std::endl;
  return ;
}

// See http://dl.acm.org/citation.cfm?id=1225842
// Basically defines a reachability metric R = Pr/Or where r(u,v) = {0,1} denoting reachability
//Pr is the num of distinct node pairs in the graph(connected) and Or is the theoretical limit i.e.
// assuming everyone in the graph is connected i.e. n(n-1)/2

void reachability_metric(route_t route_table){
  route_t::iterator it;
  path_t::iterator dit;
  std::set<std::pair<int,int> > uniq_asn_map;
  std::set<int>::iterator sit;
  std::set<int> uniq_deq; // contains entries from deque to create pairings among themselves
  std::set<int> total_uniq_asn;
  
  for(it = route_table.begin();it != route_table.end(); ++it){
    total_uniq_asn.insert(it->first);
    for(dit = it->second.begin();dit != it->second.end(); ++dit){
      total_uniq_asn.insert(dit->first);
      uniq_asn_map.insert(std::make_pair(it->first,dit->first));      
      for(int i =0; i<dit->second.size();++i){
	total_uniq_asn.insert(dit->second[i]);
	uniq_asn_map.insert(std::make_pair(it->first,dit->second[i]));
	uniq_asn_map.insert(std::make_pair(dit->first,dit->second[i]));
	for(sit = uniq_deq.begin(); sit != uniq_deq.end(); ++sit){
	  uniq_asn_map.insert(std::make_pair(*sit,dit->second[i]));
      }
	uniq_deq.insert(dit->second[i]);
      }
      uniq_deq.clear();
    }
  }
  std::cout<<"Total number of ASN's "<<total_uniq_asn.size()<<std::endl;
  std::cout<<"Number of Pairs "<<uniq_asn_map.size()<<std::endl;
}

void reachability_metric_failure(route_t route_tab, int fnode){
  route_t route_table(route_tab);
  route_t::iterator it;
  std::map<int, std::deque<int> >::iterator dit,ditp;
  std::set<std::pair<int,int> > uniq_asn_map;
  std::set<int>::iterator sit;
  std::set<int> uniq_deq; // contains entries from deque to create pairings among themselves
  std::set<int> total_uniq_asn;
  int count(0);

  for(it = route_table.begin();it != route_table.end(); ++it){
    ditp = it->second.find(fnode);
    if(ditp!= it->second.end()){
    it->second.erase(it->second.find(fnode));
    }
    for(dit = it->second.begin();dit != it->second.end(); ++dit){
      for(int i =0; i<dit->second.size(); ++i){
	if(dit->second[i] == fnode){
	  ++count;
	  it->second.erase(dit);
	  break;
	}
      }
    }
  }

  for(it = route_table.begin();it != route_table.end(); ++it){
    total_uniq_asn.insert(it->first);
    for(dit = it->second.begin();dit != it->second.end(); ++dit){
      total_uniq_asn.insert(dit->first);
      uniq_asn_map.insert(std::make_pair(it->first,dit->first));      
      for(int i =0; i<dit->second.size();++i){
	total_uniq_asn.insert(dit->second[i]);
	uniq_asn_map.insert(std::make_pair(it->first,dit->second[i]));
	uniq_asn_map.insert(std::make_pair(dit->first,dit->second[i]));
	for(sit = uniq_deq.begin(); sit != uniq_deq.end(); ++sit){
	  uniq_asn_map.insert(std::make_pair(*sit,dit->second[i]));
      }
	uniq_deq.insert(dit->second[i]);
      }
      uniq_deq.clear();
    }
  }
  std::cout<<"Number of Routes Affected "<<count<<std::endl;
  std::cout<<"Total number of ASN's "<<total_uniq_asn.size()<<std::endl;
  std::cout<<"Number of Pairs "<<uniq_asn_map.size()<<std::endl;
}

void avg_path_length(route_t route_table){

  std::map<int,int> hist_table;
  std::map<int, int>::iterator mit;
  route_t::iterator it;
  int median(0);
  path_t::iterator dit;
  std::set<std::pair<int,int> > uniq_asn_map;
  std::set<int>::iterator sit;
  std::set<int> uniq_deq; // contains entries from deque to create pairings among themselves
  std::set<int> total_uniq_asn;
  
  for(it = route_table.begin();it != route_table.end(); ++it){
    for(dit = it->second.begin();dit != it->second.end(); ++dit){
      for(int i =0; i<dit->second.size();++i){
	if(hist_table.find(dit->second.size()) == hist_table.end()){
	  hist_table[dit->second.size()] = 0;
	}
	// std::cout<<hist_table[dit->second.size()]<<std::endl;
	++hist_table[dit->second.size()];
      }
    }
  }
  std::cout<<"Avg Path Length: "<<std::endl; 
 for(mit = hist_table.begin(); mit != hist_table.end(); ++mit){
   std::cout<<mit->first<<" "<<mit->second<<std::endl;

}
  
}
// void upd_route_table_casea(rib_table,route_table){
  
// }

